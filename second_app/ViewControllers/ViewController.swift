//
//  ViewController.swift
//  second_app
//
//  Created by Nehal Mehta on 28/03/20.
//  Copyright © 2020 Nehal Mehta. All rights reserved.
//

import UIKit
import JWTDecode

class ViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTExtField: UITextField!
    
//    let AcessToken = UserDefaults.standard.object(forKey: "loggedIn") as! String
    let currentTime = Int64(Date().timeIntervalSince1970)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        usernameTextField.placeholder = "username"
        passwordTExtField.placeholder="password"
        
       
    }

    
    override func viewDidAppear(_ animated: Bool) {
       do
                      {
                      let AcessToken = UserDefaults.standard.object(forKey: "loggedIn") as! String
                      let payload = try decode(jwt: AcessToken)
                      let expTime = payload.body["exp"] as! Int64
                      if(currentTime < expTime){
                          print("remain logged in")
                          let loginScreen = ViewController()
                          let secondScreen = LoginViewController()
                          let storyboard = UIStoryboard(name: "Main", bundle: nil)
                          let vc = storyboard.instantiateViewController(withIdentifier: "loginStoryboard") as! UIViewController
                          self.present(vc, animated: true, completion: nil)
                      }
                      else
                      {
                          print("refresh token")
                        let access_token : String = AcessToken
                                    var request = URLRequest(url:URL(string:"https://api.servetel.in/v1/auth/refresh")!)
                                    request.httpMethod = "POST"
                                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                                    request.addValue("application/json", forHTTPHeaderField: "Accept")
                                    request.addValue(access_token, forHTTPHeaderField: "authorization")
                                    print(request)
                                    print("data request printed")
                                    let session = URLSession.shared
                        session.dataTask(with: request){data, response, err in
                
                                                //Guard: ws there error ?
                                                guard(err == nil) else {
                                                    print("\(err)")
                                                    return
                                                }
                                                
                                                //Guard: check was any data returned?
                                                guard let data = data else{
                                                    print("no data return")
                                                    return
                                                }
                                                
                                               
                                                // convert Json to Object
                                                let parseResult: [String:AnyObject]!
                                                do {
                                                    print("success1 refresh")
                                                    parseResult = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as! [String: AnyObject]
                                                    print("success2 refresh ")
                                                    guard let diction = parseResult as? [String: AnyObject] else { print("error in handling")
                                                        return
                                                    }
                                                    print("success3")
                                                    print("\(diction)  is the dicionary ")
                                                    print(diction["access_token"] , "refreshed token")
                                                    
                                                   
                                                    let success = diction["success"] as! Int
                                                    print(success)
                                                    if(success == 1)
                                                    {
                                                       print("success")
                                                        UserDefaults.standard.set(diction["access_token"], forKey: "loggedIn")
                                                        DispatchQueue.main.async {
                                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                            let vc = storyboard.instantiateViewController(withIdentifier: "loginStoryboard") as! UIViewController
                                                            self.present(vc, animated: true, completion: nil)
                                                        }
                                                        
                                                    }
                                                } catch {
                                                        print("Could not parse data as Json \(data)")
                                                        return
                                                }
                                                guard let diction = parseResult as? [String: AnyObject] else { print("error in handling")
                                                    return
                                                }
                                                
                                                // check jsonDictionary
                                                guard let jsonArray = diction as? [String:AnyObject] else{
                                                    print("jsonDictionary error")
                                                    return
                                                }
                        }.resume()
                      }
                      }
                      catch
                      {
                          print("catch refresh token")
                      }
    }

    
    
    @IBAction func loginButton(_ sender: Any) {
                let email : String = self.usernameTextField.text!
                let password : String = self.passwordTExtField.text!

                    let postString = ["email":email, "password": password]
                    var request = URLRequest(url:URL(string:"https://api.servetel.in/v1/auth/login")!)
                    request.httpMethod = "POST"
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.addValue("application/json", forHTTPHeaderField: "Accept")
                    request.httpBody = try! JSONSerialization.data(withJSONObject: postString, options:.prettyPrinted)
                    print(request)
                    print("data request printed")
                    let session = URLSession.shared
                    //Post
                    session.dataTask(with: request){data, response, err in
                        
                        //Guard: ws there error ?
                        guard(err == nil) else {
                            print("\(err)")
                            return
                        }
                        
                        //Guard: check was any data returned?
                        guard let data = data else{
                            print("no data return")
                            return
                        }
                        // convert Json to Object
                        let parseResult: [String:AnyObject]!
                        do {
                            print("success1")
                            parseResult = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as! [String: AnyObject]
                            print("success2")
                            guard let diction = parseResult as? [String: AnyObject] else { print("error in handling")
                                return
                            }
                            print("success3")
                            print("\(diction)  is the dicionary ")
                            print(diction["access_token"])
                            
                            
                            let success = diction["success"] as! Int
                            print(success)
                            if(success == 1)
                            {
                               print("success")
                                UserDefaults.standard.set(diction["access_token"], forKey: "loggedIn")
                            }
                        } catch {
                                print("Could not parse data as Json \(data)")
                                return
                        }
                        guard let diction = parseResult as? [String: AnyObject] else { print("error in handling")
                            return
                        }
                        
                        // check jsonDictionary
                        guard let jsonArray = diction as? [String:AnyObject] else{
                            print("jsonDictionary error")
                            return
                        }
                        
                        print(jsonArray)
                        
                        // check jsonArray and switch to LoginViewController
                        if (jsonArray.count == 0) {
                            print("jsonArray not found")
                            return
                        }
                        let successful = jsonArray["success"] as? Int
                        if(successful == 1)
                        {
                            print("success4")
                            DispatchQueue.main.async{
                                let loginvc = LoginViewController()
                                self.performSegue(withIdentifier: "loginPlease" , sender: self)
                                print(jsonArray)
                            }
                        }
                        else
                        {
                            print("you failed")
                        }
                    }.resume()
    }
    
}


