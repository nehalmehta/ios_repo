//
//  LoginViewController.swift
//  second_app
//
//  Created by Nehal Mehta on 28/03/20.
//  Copyright © 2020 Nehal Mehta. All rights reserved.
//

import UIKit
import AVFoundation

class LoginViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
var list = [NSString] ()
var descriptionCdr = [NSString]()
var dateTime = [NSString]()
var duration = [NSInteger]()
var recording = [NSString]()

@IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var imageView: UIButton!
let AcessToken = UserDefaults.standard.object(forKey: "loggedIn") as! String
var page = 1
var audioPlayer: AVAudioPlayer!
var records: String = ""
func scrollToBottom(){
    DispatchQueue.main.async {
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
}

var player : AVPlayer?
func playSound(recordingURL : String)
    {
        print(recordingURL,"recording url print")
      guard  let url = URL(string: recordingURL)
     else
        {
          print("error to get the mp3 file")
          return
        }
        print(url, "printing url")
     do{
        try AVAudioSession.sharedInstance().setCategory(.playback , mode: .default)
         try AVAudioSession.sharedInstance().setActive(true)
         player = try AVPlayer(url: url as URL)
         guard let player = player
               else
                   {
                     return
                   }
         player.play()
      } catch let error {
        print("error")
            print(error.localizedDescription)
               }
   }


@IBAction func playBtn(_ sender: Any , recordsBtn: String) {
    print(recordsBtn, "records printed")
    print("played a button")
    playSound(recordingURL: recordsBtn)
}
override func viewDidLoad() {
    super.viewDidLoad()
    print(list, "it should be empty")
    fetchCdr(pageNo: page)
}

@IBAction func imageClicked(_ sender: Any) {
    print("image is cliccked")
    let indexPath = self.tableView.indexPathForSelectedRow!
    print(indexPath)
}

func loadMore()
{
    print(page)
    page += 1
    print(page)
    fetchCdr(pageNo: page)
}

func fetchCdr(pageNo: Int ) {
    print(pageNo)
    
    let access_token : String = UserDefaults.standard.object(forKey: "loggedIn") as! String
                                                var request = URLRequest(url:URL(string:"https://api.servetel.in/v1/call_report?page=\(pageNo)")!)
                                                request.httpMethod = "GET"
                                                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                                                request.addValue("application/json", forHTTPHeaderField: "Accept")
                                                request.addValue(access_token, forHTTPHeaderField: "authorization")
                                                print(request)
                                                print("data request printed")
                                                let session = URLSession.shared
                                    session.dataTask(with: request){data, response, err in
                            
                                                            //Guard: ws there error ?
                                                            guard(err == nil) else {
                                                                print("\(err)")
                                                                return
                                                            }
                                                            
                                                            //Guard: check was any data returned?
                                                            guard let data = data else{
                                                                print("no data return")
                                                                return
                                                            }
                                                            
                                                           
                                                            // convert Json to Object
                                                            let parseResult: [String:AnyObject]!
                                                            do {
                                                                print("success1 refresh")
                                                                parseResult = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as! [String: AnyObject]
                                                                print("success2 refresh ")
                                                                guard let diction = parseResult as? [String: AnyObject] else { print("error in handling")
                                                                    return
                                                                }
                                                                print("success3")
                                                                var tableCell = diction["data"] as! NSArray
                                                                for number in 0..<tableCell.count
                                                                {
                                                                    var tableDict = tableCell[number] as! [String : AnyObject]
                                                                    print(tableDict["call_duration"])
                                                                    self.list.append((tableDict["client_number"] as? NSString) ?? "no phone number exists")
                                                                    self.descriptionCdr.append((tableDict["description"] as? NSString) ?? "no description available")
                                                                    self.dateTime.append((tableDict["date"] as? NSString) ?? "no description available")
                                                                    self.duration.append((tableDict["call_duration"] as? NSInteger) ?? 0)
                                                                    self.recording.append((tableDict["recording_url"] as? NSString) ?? "no recording available")
                                                                }
//
                                                                print("hello :", self.recording,  "the list array",self.recording.count)
                                                                
                                                            } catch {
                                                                    print("Could not parse data as Json \(data)")
                                                                    return
                                                            }
                                                            guard let diction = parseResult as? [String: AnyObject] else { print("error in handling")
                                                                return
                                                            }
                                                            
                                                            // check jsonDictionary
                                                            guard let jsonArray = diction as? [String:AnyObject] else{
                                                                print("jsonDictionary error")
                                                                return
                                                            }
                                        
                                        DispatchQueue.main.async {
                                            self.tableView.reloadData()
                                        }
                                    }.resume()
}

 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(list.count, "heeeeeellllllo")
        return list.count
    }

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    let cell:CdrTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! CdrTableViewCell

    cell.name.text = self.list[indexPath.row] as String
    cell.name.sizeToFit()
    cell.number.text = self.descriptionCdr[indexPath.row] as String
    cell.date.text = self.dateTime[indexPath.row] as String
    cell.duration.text = String(self.duration[indexPath.row])
        
        
    print(indexPath.row, "indexPath")
    cell.tapRecognizer1.addTarget(self, action: "imageClicked:")
        records = recording[indexPath.row] as String
        
        if indexPath.row == self.list.count - 1 {
            self.loadMore()
        }
        
        
    cell.playBtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(playRecording(_:))))
    cell.playBtn.tag = indexPath.row
    
    return cell
}

@objc func playRecording(_ gesture: UITapGestureRecognizer) {
    
    guard let cellTapped = gesture.view else {
        return
    }
    
    print(cellTapped.tag)
    playSound(recordingURL: recording[cellTapped.tag] as String)
    
}

//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//
//
//    }


func getViewIndexInTableView(tableView: UITableView, view: UIView) -> IndexPath? {
  let pos = view.convert(CGPoint.zero, to: tableView)
    
    print(tableView.indexPathForRow(at: pos))
  return tableView.indexPathForRow(at: pos)
    
}

@IBAction func logoutAction(_ sender: Any) {
    
    let access_token : String = AcessToken
                                        var request = URLRequest(url:URL(string:"https://api.servetel.in/v1/auth/logout")!)
                                        request.httpMethod = "POST"
                                        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                                        request.addValue("application/json", forHTTPHeaderField: "Accept")
                                        request.addValue(access_token, forHTTPHeaderField: "authorization")
                                        print(request)
                                        print("data request printed")
                                        let session = URLSession.shared
                            session.dataTask(with: request){data, response, err in
                    
                                                    //Guard: ws there error ?
                                                    guard(err == nil) else {
                                                        print("\(err)")
                                                        return
                                                    }
                                                    
                                                    //Guard: check was any data returned?
                                                    guard let data = data else{
                                                        print("no data return")
                                                        return
                                                    }
                                                    
                                                   
                                                    // convert Json to Object
                                                    let parseResult: [String:AnyObject]!
                                                    do {
                                                        print("success1 refresh")
                                                        parseResult = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as! [String: AnyObject]
                                                        print("success2 refresh ")
                                                        guard let diction = parseResult as? [String: AnyObject] else { print("error in handling")
                                                            return
                                                        }
                                                        print("success3")
                                                        print("\(diction)  is the dicionary ")
//                                                            print(diction["access_token"] , "refreshed token")
                                                        
                                                        
                                                        let success = diction["success"] as! Int
                                                        print(success)
                                                        if(success == 1)
                                                        {
                                                           print("success")
                                                            DispatchQueue.main.async {
                                                                self.dismiss(animated: false, completion: nil)
                                                            }
                                                            UserDefaults.standard.removeObject(forKey: "loggedIn")
                                                            print(self.AcessToken, "after lofgout access token ")
                                                            print("cancel")
                                                        }
                                                    } catch {
                                                            print("Could not parse data as Json \(data)")
                                                            return
                                                    }
                                                    guard let diction = parseResult as? [String: AnyObject] else { print("error in handling")
                                                        return
                                                    }
                                                    
                                                    // check jsonDictionary
                                                    guard let jsonArray = diction as? [String:AnyObject] else{
                                                        print("jsonDictionary error")
                                                        return
                                                    }
                            }.resume()
}
}
