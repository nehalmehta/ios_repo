//
//  CdrTableViewCell.swift
//  second_app
//
//  Created by Nehal Mehta on 30/03/20.
//  Copyright © 2020 Nehal Mehta. All rights reserved.
//

import UIKit

class CdrTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var hardcodeDuration: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var playBtn: UIImageView!
    @IBOutlet weak var playBttn: UIButton!
 
        override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    internal let tapRecognizer1: UITapGestureRecognizer = UITapGestureRecognizer()
}
